/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include "Counter.hpp"

//==============================================================================
MainComponent::MainComponent (Audio& a) :   //Thread ("CounterThread"),
                                         audio(a)
{
    setSize (500, 400);
    
    threadButton.setButtonText("Toggle counter");
 
    threadButton.addListener(this);
    addAndMakeVisible(threadButton);
   // startThread();
    

}

MainComponent::~MainComponent()
{
       // stopThread(500);
}

void MainComponent::resized()
{
    threadButton.setBounds(50, 80, 200, 200);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}


//void MainComponent::run()
//{
//    int count = 0;
//    
//    while (!threadShouldExit())
//    {
//        uint32 time = Time::getMillisecondCounter();
//        std::cout << "Counter:" << count << "\n";
//        isThreadRunning();
//        Time::waitForMillisecondCounter (time + 100);
//        
//        count ++;
//    }
//}


void MainComponent::buttonClicked (Button* button_)
{
    
    
    
    Counter threadCounter;
    
    threadCounter.toggleButton();

//    if (isThreadRunning() == false)
//    {
//            startThread();
//    }
//    
//    else
//    {
//        stopThread(500);
//    }
    
    
    
}






