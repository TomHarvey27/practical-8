//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Thomas Harvey on 09/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class Counter : public Thread
{
    
public:
    
    
    Counter();
    ~Counter();
    virtual void run() override;
    void toggleButton();
    
    
    
private:
    
    
    
    
    
};


#endif /* Counter_hpp */
