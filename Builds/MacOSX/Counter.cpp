//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Thomas Harvey on 09/11/2016.
//
//

#include "Counter.hpp"


Counter::Counter(): Thread ("CounterThread")
{
    
    DBG("start thread");
    startThread();
}


Counter::~Counter()
{
    stopThread(500);
}

void Counter::run()
{
    int count = 0;
    
    DBG("RUN");
    
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        std::cout << "Counter:" << count << "\n";
        isThreadRunning();
        Time::waitForMillisecondCounter (time + 100);
        
        count ++;
    }
}



void Counter::toggleButton()
{

    
    
    if (isThreadRunning() == false)
    {
        
     
        startThread();
    }
    
    else
    {
        stopThread(500);
    }
    
    
}



